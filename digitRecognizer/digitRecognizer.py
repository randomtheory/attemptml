#!/usr/bin/env python3

import numpy as np
import csv
import matplotlib.pyplot as plt

import pandas as pd
from sklearn import svm
from sklearn.preprocessing import StandardScaler
# import tensorflow as tf
# from tensorflow import keras


# load data
trainData = pd.read_csv('./train.csv')
testData = pd.read_csv('./test.csv')

# split up Input & Output
features = trainData.columns[1:]
tmpX = trainData[features]# observation
tmpY = trainData.label # expectations
inputSize = len(tmpX)

# split X for Training & Validatiaon
ratio = .7
nSize = int(inputSize * ratio)

trainX = tmpX[:nSize] # training data
validX = tmpX[nSize:] # validation data

trainY = tmpY[:nSize]
validY = tmpY[nSize:]

# -------------------------------------
#           PREPROCESSING
# -------------------------------------
# normalization (standarlization)
stdScaler = StandardScaler()
stdScaler.fit(trainX)
trainX = stdScaler.transform(trainX)
validX = stdScaler.transform(validX)
testX = stdScaler.transform(testData) # test Set


# -------------------------------------
#       TRAINING and VALIDATION
# -------------------------------------

# supervised learning
# classification between 0 to 9
# Support Vector Machine (SVM)

classifier = svm.SVC(gamma='auto', decision_function_shape='ovo')
print("\nClassifier:\n")
print(classifier)

# Training
print("\n------- Training -------\n")
classifier.fit(trainX, trainY)

# Validatiaon
print("------ Validating ------\n")
score = classifier.score(validX, validY)

print("Validation Score: " + str(score))

# -------------------------------------
#          TESTING
# -------------------------------------
print("------ Testing ------\n")
pY = classifier.predict(testX)
numbering = np.arange(len(pY))

tmpOut = list(map((lambda x,y: str(x+1) + ',' + str(y)), numbering, pY))

with open('digitRecognizer_1.csv', 'w') as f:
    f.write("ImageId,Label\n")
    for y in tmpOut:
        f.write(y +"\n")

#    f.close()
